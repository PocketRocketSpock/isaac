package shooterphone;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JButton;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

//GUI
public class FishView extends JFrame implements Runnable{
	protected static final int WIDTH = 903;
	protected static final int HEIGHT = 478;
	int bullets = 6;
	Graphics graphics;
	BufferedImage image;
	public FishView(){
		Thread viewthread = new Thread(this, "View Thread");
		viewthread.start();
	}
	//helper to create GUI
	public void run(){
		setLayout(new BorderLayout());
		//background label
		//JFrame stuff
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(WIDTH, HEIGHT);
		setVisible(true);
		repaint();
		//dipTarget();
	}
	//need this so that super isn't called
	public void paint(Graphics g){
		//draw background
		try {
			//please leave this so its generic and works on everyones comp
			File file = new File(System.getProperty("user.dir") + "//src//res//duck-hunt.jpg");
			image = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(image, 0, 0, this);
		//draw bullets
		g.setColor(Color.BLACK);
		for(int i = 0; i < bullets; i++){
			g.fillOval(40 + i*30, 40, 10, 10);
		}
	}
	
	public void drawTarget(int x, int y){
		if (graphics == null){
			graphics = getGraphics();
		}
		int diameter = WIDTH/50;
		graphics.setColor(Color.RED);
		graphics.fillOval(x, y, 3*diameter, 3*diameter);
		graphics.setColor(Color.WHITE);
		graphics.fillOval(x + diameter/2, y + diameter/2, 2*diameter, 2*diameter);
		graphics.setColor(Color.RED);
		graphics.fillOval(x + diameter, y + diameter , diameter, diameter);
	}
	
	public void drawCrosshair(int x, int y){
		int CWIDTH = 5, CHEIGHT = 20;
		graphics.setColor(Color.RED);
		//top
		graphics.fillRect(x-7, y-CHEIGHT-7, CWIDTH, CHEIGHT);
		//bottom
		graphics.fillRect(x-7, y+7, CWIDTH, CHEIGHT);
		//left
		graphics.fillRect(x-CHEIGHT-11, y-2, CHEIGHT, CWIDTH);
		//right
		graphics.fillRect(x+3, y-2, CHEIGHT, CWIDTH);
	}
	
	public void dipTarget(){
		Random rand;
		rand = new Random();
		int randomxmotion = rand.nextInt(10) + 6;
		//to change wavelength
		int yvar = rand.nextInt(18) + 2;
		int xinit = rand.nextInt(500);
		int yinit = rand.nextInt(350) + 25;
		for(int i = xinit; i < WIDTH; i+= randomxmotion){
			//sinusoidal movement
			repaint();
			drawTarget(i, yinit + (int)(Math.sin(i/(yvar))*15));
			try {
				Thread.sleep(500L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}